import React, { useState } from "react";
import Link                  from "components/Link/Link";
import { gql, useLazyQuery } from "@apollo/client";

const FEED_SEARCH_QUERY = gql`
    query FeedSearchQuery($filter: String!) {
        feed(filter: $filter) {
            id
            links {
                id
                url
                description
                createdAt
                postedBy {
                    id
                    name
                }
                votes {
                    id
                    user {
                        id
                    }
                }
            }
        }
    }
`;

const Search = () => {
  const [searchFilter, setSearchFilter] = useState("");
  // 23) Le hook useLazyQuery équivaut au hook useQuery, à la différence qu'il n'est pas appelé au chargement initial du composant mais lorsque on l'appelle manuellement
  const [executeSearch, { data }]       = useLazyQuery(
    FEED_SEARCH_QUERY,
  );

  function _handleSubmit() {
    executeSearch({
      variables: {
        filter: searchFilter,
      }
    })
  }

  return (
    <>
      <div>
        Search
        <input
          type="text"
          onChange={(e) => setSearchFilter(e.target.value)}
        />
        <button onClick={_handleSubmit}>OK</button>
      </div>
      {
        !!data &&
        data.feed.links.map((link, index) => (
          <Link key={link.id} link={link} index={index}/>
        ))
      }
    </>
  );
};

export default Search;
