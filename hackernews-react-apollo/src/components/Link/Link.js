import React                     from "react";
import { AUTH_TOKEN }            from "constants/auth";
import { timeDifferenceForDate } from "utils";
import { LINKS_PER_PAGE }        from "constants/link";
import { gql, useMutation }      from "@apollo/client";
import { FEED_QUERY }            from "components/LinkList/LinkList";


const VOTE_MUTATION = gql`
    mutation VoteMutation($linkId: ID!) {
        vote(linkId: $linkId) {
            id
            link {
                id
                votes {
                    id
                    user {
                        id
                    }
                }
            }
            user {
                id
            }
        }
    }
`;


const Link = ({ index, link }) => {
  const [vote] = useMutation(VOTE_MUTATION, {
    variables: {
      linkId: link.id,
    },
    // 18) Quand on effecctue une mutation qui affecte une liste de données, nous avons besoin de mettre à jour manuellement le cache.
    // Cette fonction de rappel se joue après que la mutation a été complétée et nous permet de lire le cache, de le modifier et de valider les changements
    update(cache, { data: { vote } }) {
      // 19) Appel de la me2thode readQuery() de l'instance ApolloCache qui nous permet de lire l'exacte portion que nous a besoin de mettre à jour
      const { feed } = cache.readQuery({
        query: FEED_QUERY,
        variables: {
          take,
          skip,
          orderBy
        }
      });

      // 20) Une fois que l'on possède le cache, nous pouvons créer un nouveau jeu de données en incluant nos modifications
      const updatedLinks = feed.links.map(feedLink => {
        if (feedLink.id === link.id) { // current
          return {
            ...feedLink,
            votes: [feedLink.votes, vote],
          }
        }
        return feedLink;
      });

      // 21) Nous sauvegardons nos modifications grâce à la méthode writeQuery() de l'instance ApolloCache implémentée par la meéthode update()
      cache.writeQuery({
        query: FEED_QUERY,
        data: {
          feed: {
            link: updatedLinks,
          }
        },
        variables: {
          take,
          skip,
          orderBy
        }
      });
    }
  });

  const authToken = localStorage.getItem(AUTH_TOKEN);
  const take      = LINKS_PER_PAGE;
  const skip      = 0;
  const orderBy   = { createAt: "desc" };

  return (
    <div className="flex mt2 items-start">
      <div className="flex items-center">
        <span className="gray">{index + 1}.</span>
        {
          authToken && (
            <div
              className="ml1 gray f11"
              style={{ cursor: "pointer" }}
              onClick={vote}
            >
              ▲
            </div>
          )
        }
      </div>
      <div className="ml1">
        <div>
          {link.description} ({link.url})
        </div>
        {authToken && (
          <div className="f6 lh-copy gray">
            {link.votes.length} votes | by{" "}
            {link.postedBy ? link.postedBy.name : "Unknown"}{" "}
            {timeDifferenceForDate(link.createdAt)}
          </div>
        )}
      </div>
    </div>
  );
};

export default Link;
