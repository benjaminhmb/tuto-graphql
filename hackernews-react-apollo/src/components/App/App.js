import "components/App/App.css";
import { ApolloProvider } from "@apollo/client";
import { client }         from "config/apolloClient";
import Header             from "components/Header/Header";
import Router             from "components/Router/Router";
import { BrowserRouter }  from "react-router-dom";


function App() {
  return (
    // 3) Nous wrappons notre application dans un HOC et passons à notre fournisseur Apollo le client instancié précédemment dans la config.
    <ApolloProvider client={client}>
      <BrowserRouter>
        <div className="center w85">
          <Header/>
          <div className="ph3 pv1 background-gray">
            <Router/>
          </div>
        </div>
      </BrowserRouter>
    </ApolloProvider>
  );
}


export default App;
