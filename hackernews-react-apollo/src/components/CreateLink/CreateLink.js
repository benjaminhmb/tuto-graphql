import React, { useState }  from "react";
import { gql, useMutation } from "@apollo/client";
import { useHistory }       from "react-router";
import { LINKS_PER_PAGE }   from "constants/link";
import { FEED_QUERY }       from "components/LinkList/LinkList";

// 7) Création d'un document pour notre mutation GraphQL
const CREATE_LINK_MUTATION = gql`
    mutation PostMutation(
        $description: String!
        $url: String!
    ) {
        post(description: $description, url: $url) {
            id
            createdAt
            url
            description
        }
    }
`;

const CreateLink = () => {
  const history                   = useHistory();
  const [formState, setFormState] = useState({
    description: "",
    url:         "",
  });

  // 8) On passe notre document de requête ainsi que les données à fournir sous la propriété `variable`
  // La fonction createLink qui a été destructurée du retour du hook useMutation va nous permettre d'invoquer cette mutation
  const [createLink] = useMutation(CREATE_LINK_MUTATION, {
    variables: {
      description: formState.description,
      url:         formState.url,
    },
    update:    (cache, { data: { post } }) => {
      const take    = LINKS_PER_PAGE;
      const skip    = 0;
      const orderBy = { createdAt: "desc" };

      const data = cache.readQuery({
        query:     FEED_QUERY,
        variables: {
          take,
          skip,
          orderBy,
        },
      });

      cache.writeQuery({
        query:     FEED_QUERY,
        data:      {
          feed: {
            links: [post, ...data.feed.links],
          },
        },
        variables: { // 22) On passe ici les mêmes conditions de récupération qu'au départ
          take,
          skip,
          orderBy,
        },
      });
    },
    // 10) La fonction onCompleted() fournit par le hook useMutation nous permet de jouer une fonction de rappel lorsque la mutation a été jouée/complétée
    onCompleted: () => history.push("/new/1"),
  });


  function _handleSubmit(e) {
    e.preventDefault();
    // 9) Lorsque l'utilisateur validera, nous pourrons envoyé notre requête grâce à la méthode createLink()
    createLink();
  }


  return (
    <div>
      <form
        onSubmit={_handleSubmit}
      >
        <div className="flex flex-column mt3">
          <input
            className="mb2"
            value={formState.description}
            onChange={(e) =>
              setFormState({
                ...formState,
                description: e.target.value,
              })
            }
            type="text"
            placeholder="A description for the link"
          />
          <input
            className="mb2"
            value={formState.url}
            onChange={(e) =>
              setFormState({
                ...formState,
                url: e.target.value,
              })
            }
            type="text"
            placeholder="The URL for the link"
          />
        </div>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default CreateLink;
