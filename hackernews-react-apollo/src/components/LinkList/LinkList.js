import React              from "react";
import { gql, useQuery }  from "@apollo/client";
import Link               from "components/Link/Link";
import { LINKS_PER_PAGE } from "constants/link";
import { useHistory }     from "react-router";

// 4) On construit notre requête
// La bibliothèque logicielle gql utilise les gabarits étiquetés pour analyser et retourner le document de la requête GraphQL que nous définissons au sein des accents graves.
export const FEED_QUERY = gql`
    query FeedQuery(
        $take: Int
        $skip: Int
        $orderBy: LinkOrderByInput
    ) {
        feed(take: $take, skip: $skip, orderBy: $orderBy) {
            id
            links {
                id
                createdAt
                url
                description
                postedBy {
                    id
                    name
                }
                votes {
                    id
                    user {
                        id
                    }
                }
            }
            count
        }
    }
`;

const NEW_LINKS_SUBSCRIPTION = gql`
    subscription {
        newLink {
            id
            url
            description
            createdAt
            postedBy {
                id
                name
            }
            votes {
                id
                user {
                    id
                }
            }
        }
    }
`;

const NEW_VOTES_SUBSCRIPTION = gql`
    subscription {
        newVote {
            id
            link {
                id
                url
                description
                createdAt
                postedBy {
                    id
                    name
                }
                votes {
                    id
                    user {
                        id
                    }
                }
            }
            user {
                id
            }
        }
    }
`;



const getQueryVariables = (isNewPage, page) => {
  const skip    = isNewPage ? ( page - 1 ) * LINKS_PER_PAGE : 0;
  const take    = isNewPage ? LINKS_PER_PAGE : 100;
  const orderBy = { createdAt: "desc" };
  return { take, skip, orderBy };
};

const LinkList = () => {
  const history         = useHistory();
  const isNewPage       = history.location.pathname.includes(
    "new",
  );
  const pageIndexParams = history.location.pathname.split(
    "/",
  );
  const page            = parseInt(
    pageIndexParams[pageIndexParams.length - 1],
  );

  const pageIndex = page ? ( page - 1 ) * LINKS_PER_PAGE : 0;

  /**
   * 5) On utilise le hook useQuery (en passant un document de requête GraphQL en argument) pour récupérer les éventuelles données et les éventuelles erreurs
   * Ce hook retourne plusieurs propriétés utiles dont :
   *    - loading : qui est un booléen qui permet de savoir si une requête est toujours en chargement
   *    - error :   qui permet de connaître ce qui ne va pas dans le cas d'une requête tombée en échec
   *    - data :    qui retourne les données reçues du serveur
   * Cf. les autres propriétés : https://www.apollographql.com/docs/react/data/queries/#render-prop
   *
   * 28) Le hook useQuery() nous donne accès à une fonction appelée subscribeToMore()
   */
  const { data, loading, error, subscribeToMore } = useQuery(FEED_QUERY, { variables: getQueryVariables(isNewPage, page) }); // 32) Passage des variables de tri (pagination)

  /**
   * 29) La fonction subscribeToMore() prend un seul argument de type objet.
   * Cette objet requiert une configuration sur la façon d'écouter et de répondre à une souscription.
   */
  subscribeToMore({
    document: NEW_LINKS_SUBSCRIPTION, // 30) Nous passons un document en premier argument qui sera le document de requête de notre souscription
    // 31) La méthode updateQuery() nous permet de mettre à jour notre cache à l'instar de ce que l'on peut retrouver avec la méthode update() pour les mutations.
    updateQuery: (prev, { subscriptionData }) => {
      if (!subscriptionData.data) return prev;
      const newLink = subscriptionData.data.newLink;
      const exists = prev.feed.links.find(
        ({ id }) => id === newLink.id
      );
      if (exists) return prev;

      return Object.assign({}, prev, {
        feed: {
          links: [newLink, ...prev.feed.links],
          count: prev.feed.links.length + 1,
          __typename: prev.feed.__typename
        }
      });
    }
  });

  subscribeToMore({
    document: NEW_VOTES_SUBSCRIPTION
  });

  const getLinksToRender = (isNewPage, data) => {
    if (isNewPage) {
      return data.feed.links;
    }
    const rankedLinks = data.feed.links.slice();
    rankedLinks.sort(
      (l1, l2) => l2.votes.length - l1.votes.length
    );
    return rankedLinks;
  };

  return (
    <div>
      {
        // 6) On utilise ici directement les informations retournées par notre requête sous le même format passé à cette dernière
        <>
          {loading && <p>Loading...</p>}
          {error && <pre>{JSON.stringify(error, null, 2)}</pre>}
          {data && (
            <>
              {getLinksToRender(isNewPage, data).map(
                (link, index) => (
                  <Link
                    key={link.id}
                    link={link}
                    index={index + pageIndex}
                  />
                )
              )}
              {isNewPage && (
                <div className="flex ml4 mv3 gray">
                  <div
                    className="pointer mr2"
                    onClick={() => {
                      if (page > 1) {
                        history.push(`/new/${page - 1}`);
                      }
                    }}
                  >
                    Previous
                  </div>
                  <div
                    className="pointer"
                    onClick={() => {
                      if (
                        page <=
                        data.feed.count / LINKS_PER_PAGE
                      ) {
                        const nextPage = page + 1;
                        history.push(`/new/${nextPage}`);
                      }
                    }}
                  >
                    Next
                  </div>
                </div>
              )}
            </>
          )}
        </>
      }
    </div>
  );
};

export default LinkList;
