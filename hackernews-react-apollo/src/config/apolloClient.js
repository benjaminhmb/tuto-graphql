import { ApolloClient, ApolloLink, createHttpLink, InMemoryCache, split } from "@apollo/client";
import { setContext }                                                     from "@apollo/client/link/context";
import { AUTH_TOKEN }                                                     from "constants/auth"; // 18) Voir plus a1 propos de l'authentification sur Apollo :
// https://www.apollographql.com/docs/react/networking/authentication/
/**
 * 24) Package permettant les subscriptions en GraphQL. Par défaut, les souscriptions sont implémentées au travers des Webockets.
 */
import { WebSocketLink }                                                  from "@apollo/client/link/ws";
import { getMainDefinition }                                              from "@apollo/client/utilities";

// 1) Création de l'HttpLink qui va connecter notre instance ApolloCLient avec notre API GraphQL. Ici le serveur GraphQL tourne sur l'URL http://localhost:4000.
const httpLink = createHttpLink({
  uri: "http://localhost:4000",
});

/**
 * 15) Création d'un middleware pour utiliser notre token au travers d'un lien Apollo (https://github.com/apollographql/apollo-link)
 *
 *     - Ce middleware sera invoqué à chaque fois que notre client Apollo enverra une requête au serveur ;
 *     - Les liens Apollo nous permettent de créer des middlewares qui iront modifier une requête avant son envoi ;
 *     - Les liens représentent une petite portion de code qui spécifie comment un opération GraphQL doit être gérée ;
 *
 * @type {ApolloLink}
 */
const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem(AUTH_TOKEN);
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    },
  };
});

// 25) On ajoute notre middleware (de type lien) WebSocketLink qui représente notre connexion WebSocket.
const wsLink = new WebSocketLink({
  uri:     "ws://localhost:4000/graphql", // Utilisation du protocol ws
  options: {
    reconnect: true,
    authToken: localStorage.getItem(AUTH_TOKEN),
  },
});

// 16) Nous lions nos liens en un seul grâce à l'interface ApolloLink et sa méthode statique from() (cf. https://www.apollographql.com/docs/link/composition/)
// const link = ApolloLink.from([
//   authLink,
//   httpLink,
// ]);

/**
 * 26) Nous utilisons la fonction split() pour rediriger proprement nos requêtes et mettre à jour le constructeur de ApolloClient
 *
 * Split() est utilisé pour "acheminer" une demande vers un lien spécifique du middleware.
 * Il prend trois arguments:
 *    - le premier est une fonction de test qui renvoie un booléen
 *    - Les deux autres arguments sont de nouveau du type ApolloLink.
 *
 * Si la fonction de test renvoie true, alors la requête sera acheminée vers le lien passé comme deuxième argument (i.e. le premier lien Apollo).
 * Si elle est false, au troisième arguments (i.e le deuxième lien Apollo).
 */

const link = split(
  /**
   * 27)
   * Dans ce cas, nous analysons si la requête est une opération de type souscription.
   * Si elle l'est, alors on la transmet vers notre lien Appolo WebSocket (i.e. wsLink).
   * Sinon, il s'agit d'une "query" ou d'une mutation que l'on doit redirigé vers notre second lien.
   * @param query
   * @return {boolean}
   */
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return (
      kind === "OperationDefinition" &&
      operation === "subscription"
    );
  },
  wsLink,
  authLink.concat(httpLink), // concat est une méthode d'ApolloLink permettant de joindre deux liens entre eux
);

// 2) Instancication de la classe ApolloClient avec en options notre objet HttpLink et une nouvelle instance de InMemoryCache
export const client = new ApolloClient({
  link, // 17) Nous nous assurons que le client Apollo est instancié avec les bons liens composés
  cache: new InMemoryCache(),
});
